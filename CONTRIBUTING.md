# Contrinuting
BetterWeb is an extension that aims to improve the web by strictly controlling websites. It does this by injecting [Content Security Policies](https://content-security-policy.com) and resources such as Cascading Style Sheets.

## Acceptible CDNs
- A
- B
- Other CDNs covered by Decentraleyes
- Single site CDNs such as `sstatic.net` for `stackoverflow.com`
// TODO

## Code Style
Use formatters for files commited, [JavaScript](http://jsbeautifier.org/) and [Cascading Style Sheets](https://www.freeformatter.com/css-beautifier.html)

