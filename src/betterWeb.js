console.log("[betterWeb] Starting");

var cspHeader = "Content-Security-Policy";

var listener = function(details) {
	var responseHeaders;
	switch (details.url.replace(/^(.*\/\/[^\/?#]*).*$/, "$1")) {
		case "https://www.androidpolice.com":
			console.log("[betterWeb] Caught 'androidpolice.com'");

			responseHeaders = replaceHeader(details.responseHeaders, {
				name: cspHeader,
				value: "default-src 'none'; style-src 'self'; img-src 'self'; font-src 'self';"
			});
			responseHeaders.push({
				name: "Link",
				value: `<${browser.extension.getURL("sites/www.androidpolice.com/style.css")}>; rel=stylesheet; type=text/css;`
			});
			responseHeaders.push({
				name: "Link",
				value: `<${browser.extension.getURL("sites/www.androidpolice.com/dark.css")}>; rel=alternate stylesheet; type=text/css; title=Dark`
			});

			return {
				responseHeaders: responseHeaders
			};
		case "https://arstechnica.com":
			console.log("[betterWeb] Caught 'arstechnica.com'");

			responseHeaders = replaceHeader(details.responseHeaders, {
				name: cspHeader,
				value: "default-src 'none'; style-src 'unsafe-inline' cdn.arstechnica.net; img-src cdn.arstechnica.net; child-src www.youtube.com;"
			});
			responseHeaders.push({
				name: "Link",
				value: `<${browser.extension.getURL("sites/arstechnica.com/style.css")}>; rel=stylesheet; type=text/css;`
			});
			responseHeaders.push({
				name: "Link",
				value: `<${browser.extension.getURL("sites/arstechnica.com/dark.css")}>; rel=alternate stylesheet; type=text/css; title=Dark`
			});

			return {
				responseHeaders: responseHeaders
			};
		case "https://content-security-policy.com":
			console.log("[betterWeb] Caught 'content-security-policy.com'");

			responseHeaders = replaceHeader(details.responseHeaders, {
				name: cspHeader,
				value: "default-src 'none'; style-src 'self' maxcdn.bootstrapcdn.com; font-src maxcdn.bootstrapcdn.com;"
			});

			return {
				responseHeaders: responseHeaders
			};
		case "http://www.rssboard.org":
			console.log("[betterWeb] Caught 'rssboard.org'");

			responseHeaders = replaceHeader(details.responseHeaders, {
				name: cspHeader,
				value: "default-src 'none'; style-src 'self' 'unsafe-inline'; img-src 'self';"
			});

			return {
				responseHeaders: responseHeaders
			};
		case "https://www.xda-developers.com":
			console.log("[betterWeb] Caught 'xda-developers.com'");

			responseHeaders = replaceHeader(details.responseHeaders, {
				name: cspHeader,
				value: "default-src 'none'; style-src *.xda-cdn.com; img-src *.xda-cdn.com *.googleusercontent.com;"
			});

			responseHeaders.push({
				name: "Link",
				value: `<${browser.extension.getURL("sites/www.xda-developers.com/style.css")}>; rel=stylesheet; type=text/css`
			});

			return {
				responseHeaders: responseHeaders
			};
	}
};

function replaceHeader(headers, header) {
	for (var i = 0; i < headers.length; i++) {
		if (headers[i].name.toLowerCase() === header.name.toLowerCase()) {
			headers[i].value = header.value;
			return headers;
		}
	}
	headers.push(header);
	return headers;
}

browser.webRequest.onHeadersReceived.addListener(listener, {
	urls: ["https://www.androidpolice.com/*",
		"https://arstechnica.com/*",
		"https://content-security-policy.com/*",
		"http://www.rssboard.org/*",
		"https://www.xda-developers.com/*"
	]
}, ["responseHeaders", "blocking"]);

console.log("[betterWeb] Finishing");

